#[macro_use]
extern crate serde_derive;
extern crate tokio;
extern crate bincode;
extern crate memmap;
extern crate id_tree;
extern crate regex;
#[macro_use]
extern crate log;
extern crate chrono;
extern crate timer;

#[cfg(test)]
mod tests;

#[macro_use]
pub mod p_utils; //XXX MOD containing macros must be declared first
pub mod p_tasks;
pub mod p_types;
