use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
//use std::fmt::Debug;

macro_rules! p_error {
    ( $( $x:expr ),* ) => {
        {
            use std::io::{Error,ErrorKind};
            let mut msg = String::new();

            $(
                msg.push_str(&format!("{:?} ", $x));
             )*

            let err = Box::new(Error::new(
                    ErrorKind::InvalidData,
                    format!("{}:{} -> {}", file!(), line!(), msg)
                    ));
            return Err(err);
        }
    };
}

//pub fn calculate_hash<T: Hash + Debug>(t: &T) -> u64 {
pub fn calculate_hash<T: Hash>(t: &T) -> u64 {
    let mut hasher = DefaultHasher::new();
    t.hash(&mut hasher);
    hasher.finish()
}
