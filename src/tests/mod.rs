extern crate bincode;
extern crate rand;

use super::p_types::*;
use tests::rand::prelude::random;
use tests::bincode::{serialize, deserialize};
use std::fs::*;
use std::io::Read;


#[test]
fn test_ser_deser() {
    let mut pack = Pck::new(MsgType::Transfer);
    pack.timestamp = Pck::get_unix_secs().unwrap_or(0);
    pack.id = random();
    pack.reserved = 0xdeadbeef;

    //let mut msg = Msg::new(MsgType::Request);
    pack.add_tlv(TLV::MTC(0x69));
    pack.add_tlv(TLV::ACK(None));
    pack.add_tlv(TLV::ACK(Some(0x66)));

    let mut f = File::open("/etc/fstab").unwrap();
    let mut bytes = vec![];
    f.read_to_end(&mut bytes).unwrap();
    let fpart = TLV::PART(Some(FilePart {
        id: 666,
        offset: 0,
        length: bytes.len(),
        bytes: Some(bytes),
    }));

    pack.add_tlv(fpart);
    pack.add_tlv(TLV::PART(Some(FilePart::new())));
    //pack.add_tlv(msg);

    let ser = serialize(&pack).unwrap();
    let deser: Pck = deserialize(&ser).unwrap();
    assert_eq!(
        deser,
        pack,
        "Initial Pck and Deserialised one are not equal"
    );
}
