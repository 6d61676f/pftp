extern crate pftp;
extern crate tokio;
extern crate bincode;
extern crate env_logger;

use pftp::*;
use p_tasks::*;
use p_tasks::file_server::*;
use p_types::*;
use std::net as snet;
use bincode::serialize;
use std::error::Error;
use std::env;

static DONE: bool = false;

fn _test_client() -> Result<(), Box<Error>> {
    let mut m = Pck::new(MsgType::Request);
    m.timestamp = Pck::get_unix_secs()?;
    m.id = 0x1000;
    m.reserved = 0x69;
    let ser = serialize(&m)?;
    let src: snet::SocketAddr = "0.0.0.0:0".parse()?;
    let dst: snet::SocketAddr = "0.0.0.0:5555".parse()?;
    let udp = snet::UdpSocket::bind(&src)?;
    for _ in 0..5 {
        udp.send_to(&ser, &dst)?;
        println!("Client wrote stuff");
        std::thread::sleep(std::time::Duration::from_millis(500));
    }
    Ok(())
}

fn _test_tokio() -> Result<(), Box<Error>> {
    let args: Vec<String> = std::env::args().collect();
    if args.len() <= 1 {
        println!("Running server");
        let sv = Server::new(&DONE, "0.0.0.0:5555", "0.0.0.0:5566");
        tokio::run(sv);
    } else {
        println!("Running client");
        _test_client()?;
    }
    Ok(())
}

fn _test_file_server() -> Result<(), Box<Error>> {
    let path = match env::var("HOME") {
        Ok(mut val) => {
            val.push_str("/Documents");
            val
        }
        Err(_) => {
            eprintln!("HOME var does not exist, using /tmp");
            "/tmp".to_owned()
        }
    };
    let mut fs_s = FileServerBuilder::new()
        .set_type(FileServerType::Server)
        .recurse(true)
        .set_path(&path)
        .build()?;
    let mut fs_c = FileServerBuilder::new()
        .set_type(FileServerType::Client)
        .recurse(true)
        .set_path("/tmp")
        .build()?;
    let file = String::from(".*generate_sum.*sh$");
    let id = fs_s.map_file_by_name(&file, true)?;
    fs_s.map_file(id, false)?;
    let mut file_part = FilePart::new();
    file_part.id = id;
    file_part.length = 450;
    fs_s.get_part(&mut file_part)?;
    println!("{:?}", file_part);

    let e = fs_s.get_entry(&id)?;
    fs_c.create_file(&e)?;
    fs_c.set_part(&file_part)?;
    //fs.cache_db("cache")?;
    //let fs2 = FileServerBuilder::new().set_path(&path).uncache("cache")?;
    //fs.dump();
    //println!("=============================");
    //fs2.dump();
    Ok(())
}

fn main() -> Result<(), Box<Error>> {
    env_logger::init();
    //_test_file_server()?;
    _test_tokio()?;
    //let p = Pck::new(MsgType::Request);
    //let v = serialize(&p)?;
    //println!("{:?} {}", v, v.len());
    Ok(())
}
