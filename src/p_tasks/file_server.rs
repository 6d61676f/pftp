use id_tree::{Tree, TreeBuilder, Node, NodeId};
use std::fs::{symlink_metadata, read_dir, read_link, File};
use std::path::{Path, PathBuf};
use std::error::Error;
use std::collections::{HashMap, VecDeque};
use std::sync::Arc;
use memmap::{Mmap, MmapMut};
use p_types::*;
use regex::Regex;
use std::cmp;
use bincode::{serialize, deserialize};
use std::fs;
use std::io::Write;
use std::fs::OpenOptions;

#[derive(PartialEq)]
pub enum FileServerType {
    Client,
    Server,
}

pub struct FileServerBuilder {
    max_recurse: isize,
    fs_type: FileServerType,
    path: String,
}

impl FileServerBuilder {
    pub fn new() -> FileServerBuilder {
        FileServerBuilder {
            max_recurse: -1,
            path: String::from("$HOME"),
            fs_type: FileServerType::Server,
        }
    }

    pub fn recurse(mut self, flag: bool) -> Self {
        self.max_recurse = match flag && self.fs_type == FileServerType::Server {
            false => 0,
            true => -1,
        };
        return self;
    }

    pub fn max_recurse(mut self, max: usize) -> Self {
        if self.fs_type != FileServerType::Client {
            self.max_recurse = max as isize;
        }
        return self;
    }

    pub fn set_path(mut self, path: &str) -> Self {
        self.path = String::from(path);
        return self;
    }

    pub fn set_type(mut self, fs_type: FileServerType) -> Self {
        self.fs_type = fs_type;
        if self.fs_type == FileServerType::Client {
            self.max_recurse = 0;
        }
        return self;
    }

    pub fn build(self) -> Result<FileServer, Box<Error>> {
        let mtd = symlink_metadata(&self.path)?;
        if mtd.is_dir() == false {
            //return Err(Box::new(io::Error::new(
            //io::ErrorKind::InvalidInput,
            //"Please supply directory",
            //)));
            p_error!("Please supply directory.", self.path, "is file");
        } else {
            let mut fs =
                FileServer::new(&PathBuf::from(&self.path), self.max_recurse, self.fs_type)?;
            if fs.fs_type == FileServerType::Server {
                fs.file_maps = Some(HashMap::new());
            }
            return Ok(fs);
        }
    }

    pub fn uncache(self, in_path: &str) -> Result<FileServer, Box<Error>> {
        let mut fs = FileServer::uncache_db(in_path)?;
        fs.root_path = PathBuf::from(&self.path);
        Ok(fs)
    }
}


pub struct FileServer {
    fs_type: FileServerType,
    root_path: PathBuf,
    file_maps: Option<HashMap<u64, Arc<Mmap>>>,
    file_maps_rw: Option<HashMap<u64, Arc<MmapMut>>>,
    file_entries: HashMap<u64, FileEntry>,
    file_tree: Tree<u64>,
}

#[derive(Debug, Serialize, Deserialize)]
struct FileServerCache {
    nodes: Vec<(FileEntry, u64)>, //FileEntry and ParentId
}

impl FileServer {
    fn new(path: &Path, depth: isize, fs_type: FileServerType) -> Result<FileServer, Box<Error>> {
        let mut fserver = FileServer {
            fs_type: fs_type,
            root_path: PathBuf::new(),
            file_maps: None,
            file_maps_rw: Some(HashMap::new()),
            file_entries: HashMap::new(),
            file_tree: TreeBuilder::new().build(),
        };
        fserver.root_path = PathBuf::from(path);
        info!("Setting root path {:?}", path);
        let new_root = fserver.add_item(&path, &None)?; //my version
        if depth != 0 {
            fserver.recurse(path, &Some(new_root), depth)?; //my version
        }
        Ok(fserver)
    }

    pub fn cache_db(&self, out: &str) -> Result<(), Box<Error>> {
        if self.fs_type != FileServerType::Server {
            //return Err(Box::new(io::Error::new(
            //io::ErrorKind::InvalidData,
            //format!("Cannot uncache Client FileServer"),
            //)));
            p_error!("Cannot uncache Client FileServer");
        }
        let mut cache = FileServerCache { nodes: vec![] };
        let mut coada = VecDeque::new();
        let root = self.file_tree.root_node_id().unwrap();
        let root_id = self.file_tree.get(&root)?.data();
        let entry = &self.file_entries.get(root_id).unwrap();
        coada.push_back(root);
        cache.nodes.push((entry.clone().to_owned(), 0));
        loop {
            let root = coada.pop_front().unwrap();
            let root_id = self.file_tree.get(&root)?.data();
            let mut children = self.file_tree.children_ids(root)?;
            while let Some(i) = children.next() {
                let child_id = self.file_tree.get(&i)?.data();
                let entry = &self.file_entries.get(child_id).unwrap();
                coada.push_back(i);
                cache.nodes.push((entry.clone().to_owned(), *root_id));
            }
            if coada.len() == 0 {
                break;
            }
        }
        let data = serialize(&cache)?;
        let mut f = File::create(out)?;
        f.write(&data)?;
        Ok(())
    }

    pub fn uncache_db(out: &str) -> Result<FileServer, Box<Error>> {
        let mut fs = FileServer {
            fs_type: FileServerType::Server,
            file_entries: HashMap::new(),
            file_maps_rw: None,
            file_maps: Some(HashMap::new()),
            file_tree: TreeBuilder::new().build(),
            root_path: PathBuf::new(),
        };
        let data = fs::read(out)?;
        let mut node_map = HashMap::new();
        let fsc: FileServerCache = deserialize(&data)?;
        let mut fsc_iter = fsc.nodes.into_iter();
        let (entry, p_id) = fsc_iter.next().unwrap();
        let id = entry.id;
        assert!(p_id == 0);
        warn!("Added root node {:?}", entry);
        fs.file_entries.insert(id, entry);
        let n_id = fs.file_tree
            .insert(Node::new(id), ::id_tree::InsertBehavior::AsRoot)
            .unwrap();
        node_map.insert(id, n_id);
        for (entry, parent_id) in fsc_iter {
            let id = entry.id;
            fs.file_entries.insert(id, entry);
            let n_id;
            {
                let r_id = node_map.get(&parent_id).unwrap();
                n_id = fs.file_tree
                    .insert(Node::new(id), ::id_tree::InsertBehavior::UnderNode(r_id))
                    .unwrap();
            }
            node_map.insert(id, n_id);
            warn!("Added {}", id);
        }
        Ok(fs)
    }

    ////NOTE Huge memory footprint -- drop the tree
    fn recurse(
        &mut self,
        root_path: &Path,
        root: &Option<NodeId>,
        depth: isize,
    ) -> Result<(), Box<Error>> {
        match read_dir(root_path) {
            Ok(ceva) => {
                for i in ceva {
                    if let Ok(entry) = i {
                        let path = entry.path();
                        if let Ok(new_root) = self.add_item(&path, root) {
                            if let Ok(mtd) = path.symlink_metadata() {
                                if mtd.is_dir() && depth != 0 {
                                    self.recurse(&path, &Some(new_root), depth - 1).unwrap();
                                }
                            }
                        }
                    }
                }
            }
            Err(e) => {
                error!("Error at dir({:?}) {}", root_path, e);
            }
        }
        Ok(())
    }

    //Add options for symlink traversal
    //NOTE Basically files should be added, but not folders
    fn add_item(&mut self, entry: &Path, root: &Option<NodeId>) -> Result<NodeId, Box<Error>> {
        use id_tree::InsertBehavior::*;
        let root = match root {
            Some(ref x) => UnderNode(x),
            None => AsRoot,
        };

        let mtd = entry.metadata()?;
        let mut resolved_entry = PathBuf::from(entry);

        {
            let sym_mtd = entry.symlink_metadata()?;
            //Should we allow symlink traversal??
            let is_sym = sym_mtd.file_type().is_symlink();
            if is_sym {
                if mtd.is_dir() {
                    //should we allow symlink files??
                    debug!("Will not add dir symlink {:?}", entry);
                    //return Err(Box::new(io::Error::new(
                    //io::ErrorKind::InvalidData,
                    //format!("Not processing {:?} because it's a link", entry),
                    //)));
                    p_error!(format!("Not processing {:?} because it's a link", entry));
                } else {
                    info!("Resolving symlinked file {:?}", entry);
                    let temp_entry = read_link(entry)?;
                    //resolved_entry = self.root_path.clone();
                    if temp_entry.is_relative() {
                        resolved_entry = PathBuf::from(entry).parent().unwrap().to_path_buf();
                        resolved_entry.push(temp_entry);
                    }
                }
            }
        }

        let fis = FileEntry::from(resolved_entry);
        let id = fis.id;
        //let id = calculate_hash(&resolved_entry);
        //let mut fis = FileEntry::new();
        //fis.id = id;
        //fis.size = mtd.len() as usize;
        //fis.dir = mtd.is_dir();
        //fis.name = resolved_entry;
        warn!("Adding file {:?}", fis);
        self.file_entries.insert(id, fis);
        let n_id = self.file_tree.insert(Node::new(id), root)?;
        return Ok(n_id);
    }

    pub fn dump(&self) {
        let root = self.file_tree.root_node_id().unwrap();
        for i in self.file_tree.traverse_level_order(&root).unwrap() {
            let val = self.file_entries.get(&i.data()).unwrap();
            println!("{:?}", val);
        }
    }

    pub fn create_file(&mut self, fe: &FileEntry) -> Result<(), Box<Error>> {
        assert!(fe.id != 0);
        let mut path = PathBuf::from(self.root_path.clone());
        let components = fe.name.components().skip(1);
        for i in components {
            path.push(i);
        }
        fs::create_dir_all(path.parent().unwrap())?;
        println!("{:?}", path);
        let f = OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .read(true)
            .open(path)?;
        f.set_len(fe.size as u64)?;
        let map = unsafe { MmapMut::map_mut(&f)? };
        self.file_maps_rw.as_mut().unwrap().insert(
            fe.id,
            Arc::new(map),
        );
        self.file_entries.insert(fe.id, fe.clone());
        Ok(())
    }


    pub fn map_file(&mut self, id: u64, rw: bool) -> Result<(), Box<Error>> {
        match rw {
            true => {
                if self.file_maps_rw.as_ref().unwrap().contains_key(&id) {
                    info!("File with id({}) is already mapped", id);
                    return Ok(());
                } else {
                    debug!("File id({}) not found", id);
                    //return Err(Box::new(io::Error::new(
                    //io::ErrorKind::InvalidInput,
                    //format!("ID({}) is invalid. Please call create_file", id),
                    //)));
                    p_error!(format!("ID({}) is invalid. Please call create_file", id));
                }
            }
            false => {
                if self.file_maps.as_ref().unwrap().contains_key(&id) {
                    info!("File with id({}) is already mapped", id);
                    return Ok(());
                } else {
                    if let Some(ref e) = self.file_entries.get(&id) {
                        let f = File::open(&e.name)?;
                        //NOTE Be aware that if you try to map a folder you'll
                        //get an Os error 19 no device found!!!
                        let map = unsafe { Mmap::map(&f)? };
                        self.file_maps.as_mut().unwrap().insert(id, Arc::new(map));
                        info!("Mapped file({:?})", e);
                        return Ok(());
                    } else {
                        debug!("File id({}) not found", id);
                        //return Err(Box::new(io::Error::new(
                        //io::ErrorKind::InvalidInput,
                        //format!("ID({}) is invalid", id),
                        //)));
                        p_error!(format!("ID({}) is invalid", id));
                    }
                }

            }
        }
    }

    pub fn map_file_by_name(&mut self, name: &str, ci: bool) -> Result<u64, Box<Error>> {
        let mut id: u64 = 0;
        let re: Regex;
        if ci {
            let mut ci = String::from("(?i)");
            ci.push_str(name);
            re = Regex::new(&ci).unwrap();
        } else {
            re = Regex::new(name).unwrap();
        }
        for (key, val) in &self.file_entries {
            if re.is_match(&val.name.to_str().unwrap()) && val.dir == false {
                id = *key;
                println!("Found {:?}", val);
                break;
            }
        }
        self.map_file(id, false)?;
        Ok(id)
    }

    pub fn get_entry(&self, id: &u64) -> Result<&FileEntry, Box<Error>> {
        match self.file_entries.get(id) {
            Some(ref e) => return Ok(e),
            None => {
                error!("Entry with id({}) not present", id);
                //return Err(Box::new(io::Error::new(
                //io::ErrorKind::InvalidData,
                //format!(
                //"Specified id({}) not valid",
                //id,
                //),
                //)));
                p_error!(format!(
                    "Specified id({}) not valid",
                    id,
                ));
            }
        }
    }

    pub fn get_part(&mut self, file_part: &mut FilePart) -> Result<(), Box<Error>> {
        let fid = file_part.id;
        let start = file_part.offset;

        self.map_file(fid, false)?;
        let map = self.file_maps.as_ref().unwrap().get(&fid).unwrap();

        if start > map.len() {
            //return Err(Box::new(io::Error::new(
            //io::ErrorKind::InvalidData,
            //format!(
            //"Offset({}) is bigger than file len({})",
            //start,
            //map.len()
            //),
            //)));
            p_error!(format!(
                "Offset({}) is bigger than file len({})",
                start,
                map.len()
            ));
        }

        let end = cmp::min(file_part.length + start, map.len());
        let mut bytes = vec![];

        for i in start..end {
            bytes.push(map[i]);
        }

        file_part.bytes = Some(bytes);
        file_part.length = end - start;
        Ok(())
    }

    pub fn set_part(&mut self, file_part: &FilePart) -> Result<(), Box<Error>> {
        let fid = file_part.id;
        let start = file_part.offset;
        let end = file_part.offset + file_part.length;

        self.map_file(fid, true)?;
        let mut entry = self.file_maps_rw.as_mut().unwrap().get_mut(&fid).unwrap();
        let map = Arc::get_mut(&mut entry).unwrap();

        for i in start..end {
            map[i] = file_part.bytes.as_ref().unwrap()[i];
        }

        Ok(())
    }
}
