use tokio::prelude::*;
use tokio::net::UdpSocket;
use std::sync::{Arc, Mutex};
use tokio::io as tio;
use std::net::SocketAddr;
use p_types::*;
use bincode::{serialize, deserialize};
use std::collections::HashMap;
use timer::{Timer, Guard};
use chrono::Duration;

pub mod file_server;

struct Client {
    addr: SocketAddr,
    is_alive: bool, //TODO Think of something better
}

#[derive(Debug)]
struct Event {
    sock: Option<Arc<Mutex<UdpSocket>>>,
    buffer: Pck,
    from: SocketAddr,
}

impl Event {
    pub fn new(sock: Option<&Arc<Mutex<UdpSocket>>>, buffer: Pck, from: SocketAddr) -> Event {
        Event {
            sock: sock.map_or(None, |x| Some(x.clone())),
            buffer,
            from,
        }
    }
}

//This structure should get the buffer, create decoding events,
//check file maps, dispatch response events
struct EventServer {
    clients: Arc<Mutex<HashMap<u64, Client>>>,
    ctrl: Arc<Mutex<UdpSocket>>,
    trns: Arc<Mutex<UdpSocket>>,
    events: Vec<Event>,
    response_queue: Vec<Event>,
    timer: Timer,
    timer_guard: Option<Guard>,
}

impl EventServer {
    pub fn new(control: &Arc<Mutex<UdpSocket>>, transfer: &Arc<Mutex<UdpSocket>>) -> EventServer {
        let mut es = EventServer {
            clients: Arc::new(Mutex::new(HashMap::new())),
            ctrl: Arc::clone(control),
            trns: Arc::clone(transfer),
            events: vec![],
            response_queue: vec![],
            timer: Timer::new(),
            timer_guard: None,
        };
        es.start_timer(2);
        es
    }

    fn start_timer(&mut self, secs: u8) {
        warn!("starting IS_ALIVE timer with {} secs", secs);
        assert!(secs > 0);
        let clis = self.clients.clone();
        self.timer_guard = Some(self.timer.schedule_repeating(
            Duration::seconds(secs as i64),
            move || {
                let mut clients_locked = clis.lock().unwrap();
                warn!("Acquired lock...let's check clients!");
                //Create a list of keys to remove from map
                let to_remove: Vec<_> = clients_locked
                    .iter()
                    .filter(|&(_, v)| v.is_alive == false)
                    .map(|(k, _)| k.clone())
                    .collect();
                //Remove keys from map
                for i in to_remove {
                    clients_locked.remove(&i);
                }
                //Mark unremoved keys as false for future removal
                for (_, v) in clients_locked.iter_mut() {
                    v.is_alive = false;
                }
            },
        ));

    }

    fn decode(buff: &Vec<u8>) -> Option<Pck> {
        match deserialize(buff) {
            Ok(pck) => {
                warn!("I has {:?}", pck);
                return Some(pck);
            }
            Err(e) => {
                error!("Err({:?})", e);
                return None;
            }
        }
    }

    fn encode(pack: &Pck) -> Option<Vec<u8>> {
        match serialize(pack) {
            Ok(vct) => {
                warn!("Encoded succesfully {:?}", vct);
                return Some(vct);
            }
            Err(err) => {
                error!("Could not encode {:?}", err);
                return None;
            }
        }
    }

    pub fn add_event(&mut self, buff: Vec<u8>, from: SocketAddr) -> bool {
        match EventServer::decode(&buff) {
            Some(pck) => {
                //if pck.is_control() {
                //self.events.push(Event::new(Some(&self.ctrl), pck, from));
                //} else {
                //self.events.push(Event::new(Some(&self.trns), pck, from));
                //}
                self.events.push(Event::new(None, pck, from));
                return true;
            }
            None => return false,
        }
    }

    fn process(&mut self, event: Event) -> Result<(), ()> {
        warn!("Processing event {:?}", event);
        match event.buffer.kind {
            //NOTE We are responsible with creating a PCK that is at most 512 bytes
            //We start any LIST or PART PCK by putting at the end an MTC
            //IF we succesfully send everything we remove the MTC
            MsgType::Alive => {
                debug!("Matched IS_ALIVE");
                ////Check if the client is NEW or EXISTS
                //    //IF NEW IT MEANS THAT HE EXPIRED => SEND PCK/REPLY/RST
                //    //IF EXISTS RESET IS_ALIVE TIMER

            }
            MsgType::Request => {
                debug!("Matched REQUEST");
                ////Check if CLIENT EXISTS
                //    //NO and ACK(_) => Generate ID, add Client => SEND REPLY/ACK(ID)
                ////YES
                //    //LIST(_) => Get file list => SEND PCK/REPLY/FILE_LIST
                //    //PART(_) => Get Part => PCK/TRANSFER/PART
                //        //NOTE IF PART is NOT AVAILABLE SEND Special RST
                //    //MTC(_) => ??
                ////NOTE Whenever we get a REQUEST and Client exists => Reset Timer
            }
            MsgType::Reply => {
                debug!("Matched REQUEST");
                //Should the server get a reply?
                error!("Why am i getting Replies as a server?!");
            }
            MsgType::Transfer => {
                debug!("Matched REQUEST");
                //Should the server get a transfer?
                error!("Why am i getting Transfer as a server?!");
            }
        }
        Err(())
    }

    pub fn process_events(&mut self, mut count: usize) -> usize {
        loop {
            //go through the events stored in vector
            let len = self.events.len();
            for i in (0..len).rev() {
                let e = self.events.remove(i);
                match self.process(e) {
                    Ok(_) => {
                        warn!("processed {}", i);
                    }
                    Err(e) => {
                        error!("fml {:?}", e);
                    }
                }
            }

            //create response -- add , MTC at end

            //while resulting buffer <= 1024 add stuff to it
            //create TLV for each TLV request

            //create response event

            //add it to queue
            if count == 0 {
                break;
            }
            count -= 1;
        }
        count
    }
}


pub struct Server<'a> {
    //clients: HashMap<SocketAddr, Client>,
    event_srv: EventServer,
    stop: &'a bool,
    ctrl: Arc<Mutex<UdpSocket>>,
    trns: Arc<Mutex<UdpSocket>>,
}


impl<'a> Server<'a> {
    pub fn new(done: &'a bool, control: &str, transfer: &str) -> Server<'a> {
        let _ctrl = control.parse().unwrap();
        let _trns = transfer.parse().unwrap();
        warn!("ctrl({:?}) trns({:?})", _ctrl, _trns);
        let ctrl = Arc::new(Mutex::new(UdpSocket::bind(&_ctrl).unwrap()));
        let trns = Arc::new(Mutex::new(UdpSocket::bind(&_trns).unwrap()));
        Server {
            //clients: HashMap::new(),
            stop: done,
            ctrl: Arc::clone(&ctrl),
            trns: Arc::clone(&trns),
            event_srv: EventServer::new(&ctrl, &trns),
        }
    }

    pub fn recv_msg(&mut self, arr: &mut [u8]) -> Result<Async<()>, tio::Error> {
        loop {
            let mut soc = self.ctrl.lock().unwrap();
            match soc.poll_recv_from(arr)? {
                Async::Ready((len, from)) => {
                    warn!(
                        "Received {} bytes from {:?}:{:?}",
                        len,
                        from,
                        arr[0..len].to_vec()
                    );
                    self.event_srv.add_event(arr[0..len].to_vec(), from);
                }
                Async::NotReady => {
                    break;
                }
            }
        }
        self.event_srv.process_events(5);
        if *self.stop == true {
            return Ok(Async::Ready(()));
        } else {
            Ok(Async::NotReady)
        }
    }
}

impl<'a> Future for Server<'a> {
    type Item = ();
    type Error = ();
    fn poll(&mut self) -> Result<Async<Self::Item>, Self::Error> {
        let mut arr = Box::new([0u8; 1024]);
        match self.recv_msg(&mut arr[..]).unwrap() {
            Async::Ready(_) => return Ok(Async::Ready(())),
            Async::NotReady => return Ok(Async::NotReady),
        }
    }
}
