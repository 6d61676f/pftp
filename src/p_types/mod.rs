use std::time::{SystemTime, UNIX_EPOCH, SystemTimeError};
use std::path::PathBuf;
use std::fs::symlink_metadata;

use p_utils::calculate_hash;

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct Pck {
    pub timestamp: u64,
    pub id: u32,
    pub reserved: u32,
    pub kind: MsgType,
    pub msgs: Option<Vec<TLV>>,
}

impl Pck {
    pub fn new(kind: MsgType) -> Pck {
        let vector = match kind {
            MsgType::Alive => None,
            _ => Some(vec![]),
        };
        Pck {
            timestamp: 0,
            id: 0,
            reserved: 0,
            kind,
            msgs: vector,
        }
    }

    pub fn get_unix_secs() -> Result<u64, SystemTimeError> {
        let rez = match SystemTime::now().duration_since(UNIX_EPOCH) {
            Err(e) => Err(e),
            Ok(o) => Ok(o.as_secs()),
        };
        rez
    }

    fn sanity(&self, entry: &TLV) -> bool {
        let ok = match self.kind {
            MsgType::Request | MsgType::Reply => {
                match entry {
                    TLV::PART(_) | TLV::LIST(_) | TLV::ACK(_) => true,
                    _ => false,
                }
            }
            MsgType::Transfer => {
                match entry {
                    TLV::PART(_) | TLV::END(_) | TLV::MTC(_) => true,
                    _ => false,
                }
            }
            MsgType::Alive => false,
        };
        return ok;
    }

    //TODO When a client connects he sends a Request with ACK inside
    //which should be CONTROL!!!!!
    pub fn is_control(&self) -> bool {
        match self.kind {
            MsgType::Request => {
                trace!("Matching Transfer Event");
                return false;
            }
            _ => {
                trace!("Matching Control Event");
                return true;
            }
        }
    }

    pub fn add_tlv(&mut self, entry: TLV) -> bool {
        if self.sanity(&entry) == true {
            self.msgs.as_mut().unwrap().push(entry);
            return true;
        } else {
            return false;
        }
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum MsgType {
    Alive,
    Request,
    Reply,
    Transfer,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum TLV {
    ///used as Connection, Ack
    ACK(Option<u32>),
    ///used to reset client if something is wrong -- usize is reason
    RST(usize),
    ///used to request or reply file list
    LIST(Option<FileEntry>),
    ///request or reply file entry
    PART(Option<FilePart>),
    ///end of transmission for certain inode
    END(u64),
    ///more to come for certain inode
    MTC(u64),
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct FileEntry {
    pub id: u64,
    pub name: PathBuf,
    pub size: usize,
    pub dir: bool,
}

impl FileEntry {
    pub fn new() -> FileEntry {
        FileEntry {
            id: 0,
            name: PathBuf::new(),
            size: 0,
            dir: false,
        }
    }
}

impl<'a> From<&'a PathBuf> for FileEntry {
    fn from(s: &'a PathBuf) -> FileEntry {
        let mut fs = FileEntry::new();
        let symlink_mtd = symlink_metadata(&s).unwrap();
        fs.name = s.clone();
        fs.dir = symlink_mtd.is_dir();
        fs.size = symlink_mtd.len() as usize;
        fs.id = calculate_hash(&fs.name);
        return fs;
    }
}

//TODO Investigate long uncache time
//Probably from symlink resolving and such
impl From<PathBuf> for FileEntry {
    fn from(s: PathBuf) -> FileEntry {
        let mut fs = FileEntry::new();
        if let Ok(symlink_mtd) = symlink_metadata(&s) {
            fs.name = s;
            fs.dir = symlink_mtd.is_dir();
            fs.size = symlink_mtd.len() as usize;
            fs.id = calculate_hash(&fs.name);
        } else {
            debug!("Could not read {:?}", s);
        }
        return fs;
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct FilePart {
    pub id: u64,
    pub offset: usize,
    pub length: usize,
    pub bytes: Option<Vec<u8>>,
}

impl FilePart {
    pub fn new() -> FilePart {
        FilePart {
            id: 0,
            offset: 0,
            length: 0,
            bytes: None,
        }
    }
}
