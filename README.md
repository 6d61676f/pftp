#   _PFTP_ - P File Transfer Program

###  Client-Server application used for simple file transfer developed in Rust as a learner's demo

#### Simple scenario

```
+---------+                                   +---------+
| Client  |                                   | Server  |
+---------+                                   +---------+
     |                                             |
     |                                             | 1. Start, initialize, wait
     |                                             |---------------------------
     |                                             |                          |
     |                                             |<--------------------------
     |                                             |
     | 1. Start, initialize, identify server       |
     |--------------------------------------       |
     |                                     |       |
     |<-------------------------------------       |
     |                                             |
     | 2. Connect                                  |
     |-------------------------------------------->|
     |                                             |
     |                      3. Acknowledge Connect |
     |<--------------------------------------------|
     |                                             |
     |                                             | 4. Start IS_ALIVE request handler for this client
     |                                             |--------------------------------------------------
     |                                             |                                                 |
     |                                             |<-------------------------------------------------
     |                                             |
     | 4. Start IS_ALIVE response handler          |
     |-----------------------------------          |
     |                                  |          |
     |<----------------------------------          |
     |                                             |
     | 5. Send me your file list                   |
     |-------------------------------------------->|
     |                                             |
     |                     6. Here is my file list |
     |<--------------------------------------------|
     |                                             |
     | 7. User selects files and send request      |
     |---------------------------------------      |
     |                                      |      |
     |<--------------------------------------      |
     |                                             |
     | 8. Send my <x,y,...>                        |
     |-------------------------------------------->|
     |                                             |
     |                        9. Sending <x> -- OK |
     |<--------------------------------------------|
     |                                             |
     | 10. Receiving <x> -- OK                     |
     |------------------------                     |
     |                       |                     |
     |<-----------------------                     |
     |                                             |
     |        9. Sending <y> -- Some part got lost |
     |<--------------------------------------------|
     |                                             |
     | 10. Receiving <y> -- A part is lost         |
     |------------------------------------         |
     |                                   |         |
     |<-----------------------------------         |
     |                                             |
     | 8. Send me lost part of <y>                 |
     |-------------------------------------------->|
     |                                             |
     |                         9. Sending <y,part> |
     |<--------------------------------------------|
     |                                             |
     | 11. <Quit>                                  |
     |-----------                                  |
     |          |                                  |
     |<----------                                  |
     |                                             |
     |                                             | 11. Remove dead client
     |                                             |-----------------------
     |                                             |                      |
     |                                             |<----------------------
     |                                             |
```

___Technical Highlights:___

*  IPC
    -   [?] mpsc
    -   [?] local sockets
    -   [?] SERVER: __1__ worker for control messages, __N__ for each client request
    -   [?] CLIENT: __1__ worker for control messages, __1__ for data transfer
    -   [?] ___WHAT IF WE WANT A CLIENT TO COMMUNICATE WITH MULTIPLE SERVERS?!?!___
    -   [?] IS_ALIVE Messages determine if the channel is still UP - _5 Seconds?_

*  Threading
    -   [?] each client shall be handled on another thread.
The problem is that multiple threads shall access same memory maps, but _probably and hopefully_ not
at the same time. -- ___possible bottlenecks___
    -   [?] ___what to do if a client crashes and comes back up ??___

*  Message passing
    -   [?] messages encoded by TLVs and SUB-TLVs
        *   TLV:
            *   ALIVE
            *   Request: Connect, File List, Retransmit
            *   Reply: Ack Connect, File List
            *   Transfer: File Part, More to Come, End

*  Socket Communication
    -   [?] UDP sockets -- need to handle out-of-order and data corruption

*  Data serialization and transfer
    -   [?] serde with bincode and cap'n'proto?
    -   [?] define structures for the above message types
    -   [?] send UDP packets of 1024 bytes

*  Memory mappings
    -   [?] mmap files into memory when accesing them
    -   [?] send chuncks of determined size

*  Socket multiplexing
    -   [?] select/epoll

*  User interface on client-side
    -   [?] tui

Steps in Progress:

1.   [ ] Create necessary structures and TLVs
2.   [ ] Create server and client entrypoints
3.   [ ] Design and Implement Message Passing and IPC
4.   [ ] Test Communication

(1) Structures and modelling

All packets contain timestamp(usize), node-id(usize), and reserved(32 bytes)


**Be aware of the fact that inodes are linux specific**

**We should identify files in another manner**

**Also how do we keep track of files?**

-   Alive: No additional payload
-   Request:
    -   Connection: no payload
    -   File List: no payload
    -   Retransmit: inode, offset, size
    -   More to come
    -   End
-   Reply:
    -   ACK Connection: new node-id(usize)
    -   RST Connection: reason(enum)
    -   File List: file name, size, inode
    -   More to Come
    -   End
-   Transfer:
    -   File Part: (inode, offset, size) + data
    -   More to Come: inode
    -   End: inode
